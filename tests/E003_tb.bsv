//!topmodule mkE003_tb
import ESeries::*;

module mkE003_tb(Empty);
    let soc <- mkE003();

    rule run_it;
        // Required for test to pass.
        $display(">>>PASS");
        $finish();
    endrule
endmodule
