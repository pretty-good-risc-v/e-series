//!topmodule mkE001_tb
import ESeries::*;

module mkE001_tb(Empty);
    let soc <- mkE001();

    rule run_it;
        // Required for test to pass.
        $display(">>>PASS");
        $finish();
    endrule
endmodule
