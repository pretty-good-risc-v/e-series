import Cpu::*;
export CpuCfg(..), mkSoc, SocCfg(..), Soc(..);

//
// SocConfig
//
typedef struct {
    CpuCfg#(xlen, ifetchByteCount) cpu_cfg;
} SocCfg#(numeric type xlen, numeric type ifetchByteCount);

//
// Soc
//
interface Soc#(numeric type xlen, numeric type ifetchByteCount);
endinterface

//
// mkSoc()
//
module mkSoc#(SocCfg#(xlen, ifetchByteCount) cfg)(Soc#(xlen, ifetchByteCount));
    let cpu <- mkCpu(cfg.cpu_cfg);
endmodule
