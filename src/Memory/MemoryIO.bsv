import ClientServer::*;
import GetPut::*;
import Memory::*;

typedef struct {
    Bit#(dataSz) data;
    Bool accessFault;   // The memory request faulted
} FallibleMemoryResponse#(numeric type dataSz) deriving(Bits, Eq, FShow);

//
// ReadWriteMemory
//
typedef Client#(MemoryRequest#(addrSz, dataSz), FallibleMemoryResponse#(dataSz)) ReadWriteMemoryClient#(numeric type addrSz, numeric type dataSz);
typedef Server#(MemoryRequest#(addrSz, dataSz), FallibleMemoryResponse#(dataSz)) ReadWriteMemoryServer#(numeric type addrSz, numeric type dataSz);

//
// ReadOnlyMemory
//
typedef struct {
    Bit#(TDiv#(dataSz,8)) byteen;
    Bit#(addrSz) address;
} ReadOnlyMemoryRequest#(numeric type addrSz, numeric type dataSz) deriving(Bits, Eq, FShow);

typedef Client#(ReadOnlyMemoryRequest#(addrSz, dataSz), FallibleMemoryResponse#(dataSz)) ReadOnlyMemoryClient#(numeric type addrSz, numeric type dataSz);
typedef Server#(ReadOnlyMemoryRequest#(addrSz, dataSz), FallibleMemoryResponse#(dataSz)) ReadOnlyMemoryServer#(numeric type addrSz, numeric type dataSz);
