import RV_ISA::*;

import ClientServer::*;
import GetPut::*;
import Vector::*;

interface GprReadPort#(numeric type xlen);
    method Bit#(xlen) read(RVGPRIndex index);
endinterface

interface GprWritePort#(numeric type xlen);
    method Action write(RVGPRIndex index, Bit#(xlen) value);
endinterface

interface GprFileIfc#(numeric type xlen);
    interface GprReadPort#(xlen) gprReadPort1;
    interface GprReadPort#(xlen) gprReadPort2;

    interface GprWritePort#(xlen) gprWritePort;
endinterface

typedef struct {
} GprFileCfg#(numeric type xlen);

module mkGprFile#(GprFileCfg#(xlen) cfg)(GprFileIfc#(xlen));
    Vector#(32, Reg#(Bit#(xlen))) reg_file <- replicateM(mkReg(0));

    interface GprReadPort gprReadPort1;
        method Bit#(xlen) read(RVGPRIndex index);
            return reg_file[index];
        endmethod
    endinterface

    interface GprReadPort gprReadPort2;
        method Bit#(xlen) read(RVGPRIndex index);
            return reg_file[index];
        endmethod
    endinterface

    interface GprWritePort gprWritePort;
        method Action write(RVGPRIndex index, Bit#(xlen) value);
            if (index != 0) begin
                reg_file[index] <= value;
            end
        endmethod
    endinterface
endmodule
