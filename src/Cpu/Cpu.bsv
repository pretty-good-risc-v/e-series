//!submodule CSRs
import FIFO::*;
import GetPut::*;
import StmtFSM::*;

import Caches::*;
import GprFile::*;
import PipelineRegisters::*;

export CpuCfg(..), CpuIfc, mkCpu;

//
// CpuConfig
//
typedef struct {
    Bit#(xlen) initial_program_counter;
} CpuCfg#(numeric type xlen, numeric type icacheFetchByteCount);

//
// Cpu
//
interface CpuIfc#(numeric type xlen, numeric type icacheFetchByteCount);
    interface Get#(ICacheRequest#(xlen)) getCacheRequest;
    interface Put#(ICacheResponse#(icacheFetchByteCount)) putCacheResponse;
endinterface

//
// State
//
typedef enum {
    RESET,
    INITIALIZING,
    FETCH,
    DECODE,
    EXECUTE,
    MEMORY_ACCESS,
    WRITEBACK
} State deriving(Bits, Eq, FShow);

//
// mkCpu()
//
module mkCpu#(CpuCfg#(xlen, icacheFetchByteCount) cfg)(CpuIfc#(xlen, icacheFetchByteCount));
    // Cache FIFOs
    FIFO#(ICacheRequest#(xlen)) icacheRequests <- mkFIFO;
    FIFO#(ICacheResponse#(icacheFetchByteCount)) icacheResponses <- mkFIFO;

    // State
    Reg#(State)         state  <- mkReg(RESET);

    // Pipeline registers
    Reg#(Bit#(xlen))    pc     <- mkReg(cfg.initial_program_counter);
    Reg#(IF_ID#(xlen))  if_id  <- mkReg(defaultValue);
    Reg#(ID_EX#(xlen))  id_ex  <- mkReg(defaultValue);
    Reg#(EX_MEM#(xlen)) ex_mem <- mkReg(defaultValue);
    Reg#(MEM_WB#(xlen)) mem_wb <- mkReg(defaultValue);
    Reg#(WB_OUT#(xlen)) wb_out <- mkReg(defaultValue);

    // General purpose register (GPR) file
    GprFileCfg#(xlen)   gpr_cfg = GprFileCfg {};
    GprFileIfc#(xlen)   gprfile <- mkGprFile(gpr_cfg);

    //
    // INITIALIZATION
    //
    Reg#(Bit#(10)) gprInitIndex <- mkRegU;
    Stmt initializationStatements = (seq
        //
        // Zero the GPRs
        //
        for (gprInitIndex <= 0; gprInitIndex <= 32; gprInitIndex <= gprInitIndex + 1)
            gprfile.gprWritePort.write(truncate(gprInitIndex), 0);

        state <= FETCH;
    endseq);

    FSM initializationMachine <- mkFSMWithPred(initializationStatements, state == INITIALIZING);

    rule initialization(state == RESET);
        state <= INITIALIZING;
        initializationMachine.start;
    endrule

    interface getCacheRequest = toGet(icacheRequests);
    interface putCacheResponse = toPut(icacheResponses);
endmodule
