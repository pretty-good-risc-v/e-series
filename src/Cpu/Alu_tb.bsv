import Alu::*;
import IsaCfg::*;
import RV_ISA::*;

import Assert::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) stepNumber <- mkReg(0);

    // 32 bit ALU
    AluCfg#(32) alucfg32 = AluCfg {
    };
    AluIfc#(32) alu32 <- mkAlu(alucfg32);

    // 64 bit ALU
    AluCfg#(64) alucfg64 = AluCfg {
    };
    AluIfc#(64) alu64 <- mkAlu(alucfg64);

    (* no_implicit_conditions *)
    rule test;
        case(stepNumber)
            0: begin
                Maybe#(Bit#(32)) result32 = execute(alu_ADD, 3, 1);
                dynamicAssert(isValid(result32), "ALU32 - add returned invalid");
                dynamicAssert(result32.Valid == 4, "ALU32 - add result incorrect");

                Maybe#(Bit#(64)) result64 = execute(alu_ADD, 3, 1);
                dynamicAssert(isValid(result64), "ALU64 - add incorrect");
                dynamicAssert(result32.Valid == 4, "ALU64 - add result incorrect");
            end

            default: begin
                dynamicAssert(stepNumber == 1, "ALU - not all tests run");
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_step_number;
        stepNumber <= stepNumber + 1;
    endrule
endmodule