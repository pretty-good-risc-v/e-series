typedef struct {
    // userLevelInterruptsSupported - N extension
    Bool extN;

    // supervisorModeSupported - S extension
    Bool extS;

    // userModeSupported - U extension
    Bool extU;
} IsaCfg#(numeric type xlen);

typedef Bit#(2) MXL;
MXL mxl_32bit  = 2'b01;
MXL mxl_64bit  = 2'b10;
MXL mxl_128bit = 2'b11;

instance DefaultValue#(IsaCfg#(xlen));
    defaultValue = IsaCfg {
        extN: False,
        extS: False,
        extU: False
    };
endinstance

// Bits#() instance - serializing to Bit#() creates a MISA register compatible bit pattern.
instance Bits#(IsaCfg#(64), 64);
    function Bit#(64) pack(IsaCfg#(64) misa);
        MXL mxl = mxl_64bit;
        return {
            mxl,
            36'b0,
            1'b0,   // pack(misa.extZ),
            1'b0,   // pack(misa.extY),
            1'b0,   // pack(misa.extX),
            1'b0,   // pack(misa.extW),
            1'b0,   // pack(misa.extV),
            pack(misa.extU),
            1'b0,   // pack(misa.extT),
            pack(misa.extS),
            1'b0,   // pack(misa.extR),
            1'b0,   // pack(misa.extQ),
            1'b0,   // pack(misa.extP),
            1'b0,   // pack(misa.extO),
            pack(misa.extN),
            1'b0,   // pack(misa.extM),
            1'b0,   // pack(misa.extL),
            1'b0,   // pack(misa.extK),
            1'b0,   // pack(misa.extJ),
            1'b1,   // pack(misa.extI),   // Always 1 to indicate RV32I, RV64I, RV128I
            1'b0,   // pack(misa.extH),
            1'b0,   // pack(misa.extH),
            1'b0,   // pack(misa.extF),
            1'b0,   // pack(misa.extE),
            1'b0,   // pack(misa.extD),
            1'b0,   // pack(misa.extC),
            1'b0,   // pack(misa.extB),
            1'b0    // pack(misa.extA)           
        };
    endfunction

    function IsaCfg#(64) unpack(Bit#(64) value);
        return defaultValue;
    endfunction
endinstance

// Bits#() instance - serializing to Bit#() creates a MISA register compatible bit pattern.
instance Bits#(IsaCfg#(32), 32);
    function Bit#(32) pack(IsaCfg#(32) misa);
        MXL mxl = mxl_32bit;
        return {
            mxl,
            4'b0,
            1'b0,   // pack(misa.extZ),
            1'b0,   // pack(misa.extY),
            1'b0,   // pack(misa.extX),
            1'b0,   // pack(misa.extW),
            1'b0,   // pack(misa.extV),
            pack(misa.extU),
            1'b0,   // pack(misa.extT),
            pack(misa.extS),
            1'b0,   // pack(misa.extR),
            1'b0,   // pack(misa.extQ),
            1'b0,   // pack(misa.extP),
            1'b0,   // pack(misa.extO),
            pack(misa.extN),
            1'b0,   // pack(misa.extM),
            1'b0,   // pack(misa.extL),
            1'b0,   // pack(misa.extK),
            1'b0,   // pack(misa.extJ),
            1'b1,   // pack(misa.extI),   // Always 1 to indicate RV32I, RV64I, RV128I
            1'b0,   // pack(misa.extH),
            1'b0,   // pack(misa.extH),
            1'b0,   // pack(misa.extF),
            1'b0,   // pack(misa.extE),
            1'b0,   // pack(misa.extD),
            1'b0,   // pack(misa.extC),
            1'b0,   // pack(misa.extB),
            1'b0    // pack(misa.extA)           
        };
    endfunction

    function IsaCfg#(32) unpack(Bit#(32) value);
        return defaultValue;
    endfunction
endinstance