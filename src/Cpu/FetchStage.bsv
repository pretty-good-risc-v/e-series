import IsaCfg::*;
import MemoryIO::*;
import PipelineRegisters::*;
import RV_ISA::*;
import Trap::*;

import Assert::*;
import ClientServer::*;
import FIFOF::*;
import GetPut::*;
import Memory::*;

typedef ReadOnlyMemoryRequest#(xlen, 32) InstructionMemoryRequest#(numeric type xlen);

interface FetchModuleIfc#(numeric type xlen);
    method ActionValue#(IF_ID#(xlen)) step(PC_IF#(xlen) pc_if);

    interface ReadOnlyMemoryClient#(xlen, 32) memoryClient;
endinterface

module mkFetchModule#(IsaCfg#(xlen) cfg)(FetchModuleIfc#(xlen));
    Reg#(ProgramCounter#(xlen)) updatedPc <- mkRegU; // Holds the updated PC that is calculated when a fetch is issued 
                                                     // that is used to update the PC when the fetch completes.

    Reg#(Epoch) requestEpoch <- mkRegU; // Holds the epoch of the initiating request.

    // Memory request output
    Reg#(Maybe#(InstructionMemoryRequest#(xlen))) inflightMemoryRequest <- mkReg(tagged Invalid);
    Wire#(InstructionMemoryRequest#(xlen)) memoryRequest <- mkWire;

    // Memory response input (FIFO)
    // Note: This is an unguarded FIFO so status must be checked before attempting to enq() and deq().
    FIFOF#(FallibleMemoryResponse#(32)) memoryResponses <- mkUGFIFOF1;

    //
    // processMemoryResponse - Takes a memory response and returns an IF_ID containing
    //                         the encoded instruction (or a trap if the original request
    //                         was denied by the memory system)
    //
    function IF_ID#(xlen) processMemoryResponse(FallibleMemoryResponse#(32) response);    
        IF_ID#(xlen) if_id = defaultValue;
        if_id.common.pc = inflightMemoryRequest.Valid.address;
        if_id.npc = updatedPc + 4;
        if_id.epoch = requestEpoch;

        if (!response.accessFault) begin
            if_id.common.ir.value = response.data;
            if_id.common.trap = tagged Invalid;
        end else begin
            Trap#(xlen) trap = Trap {
                cause: exception_INSTRUCTION_ACCESS_FAULT,
                isInterrupt: False,
                tval: 0
            };

            if_id.common.trap = tagged Valid(trap);
        end
        return if_id;
    endfunction

    //
    // step - Execute fetch step by sending an instruction memory request if one isn't already in flight and
    //        responding to any responses that have returned.  If a request is in flight, a bubble is returned.
    //
    method ActionValue#(IF_ID#(xlen)) step(PC_IF#(xlen) pc_if);
        IF_ID#(xlen) if_id = defaultValue;

        // If there's an active request, handle it if it's returned
        if (isValid(inflightMemoryRequest)) begin
            if (memoryResponses.notEmpty) begin
                if_id = processMemoryResponse(memoryResponses.first);
                memoryResponses.deq;
                inflightMemoryRequest <= tagged Invalid;
            end else begin
                // Memory request is in flight but hasn't returned yet.
            end
        end else begin
            // No memory request is in flight...

            // Determine the PC the instruction will be fetched from.  It's either the calculated PC
            // or a redirected PC (due to a branch, jump, exception, etc)
            ProgramCounter#(xlen) updatedPc_ = fromMaybe(pc_if.pc, pc_if.redirection);

            // If a redirection was specified, reflect that with a change in the epoch.
            let epoch = pc_if.epoch;
            if (isValid(pc_if.redirection)) begin
                epoch = ~epoch;
            end

            requestEpoch <= epoch;

            // Check for a misaligned request
            if (updatedPc_[1:0] != 0) begin
                // Address request was misaligned...
                Trap#(xlen) trap = Trap {
                    cause: exception_INSTRUCTION_ADDRESS_MISALIGNED,
                    isInterrupt: False,
                    tval: 0
                };
                if_id.common.pc = updatedPc_;
                if_id.common.trap = tagged Valid(trap);
            end else begin
                // Construct a memory request and send it out.
                InstructionMemoryRequest#(xlen) request = ReadOnlyMemoryRequest {
                    address: updatedPc_,
                    byteen: 'b1111
                };
                memoryRequest <= request;
                inflightMemoryRequest <= tagged Valid request;
            end

            updatedPc <= updatedPc_;
        end

        return if_id;
    endmethod

    interface ReadOnlyMemoryClient memoryClient;
        interface Get request = toGet(memoryRequest);
        interface Put response;
            method Action put(FallibleMemoryResponse#(32) response);
                dynamicAssert(memoryResponses.notFull, "FetchStage - attempt to put a memory respnose on a full queue");
                memoryResponses.enq(response);
            endmethod
        endinterface
    endinterface
endmodule

interface FetchStageIfc#(numeric type xlen);
    interface Put#(PC_IF#(xlen)) put;
    interface Get#(IF_ID#(xlen)) get;

    interface ReadOnlyMemoryClient#(xlen, 32) memoryClient;
endinterface

module mkFetchStage#(IsaCfg#(xlen) cfg)(FetchStageIfc#(xlen));
    //
    // State
    //
    Wire#(PC_IF#(xlen)) pc_if <- mkWire;
    Reg#(IF_ID#(xlen))  if_id <- mkRegU;
    FetchModuleIfc#(xlen) fetchModule <- mkFetchModule(cfg);

    //
    // Rules
    //
    (* no_implicit_conditions *)
    rule step;
        let if_id_ <- fetchModule.step(pc_if);
        if_id <= if_id_;
    endrule

    //
    // Interfaces
    //
    interface Put put = toPut(asIfc(pc_if));
    interface Get get = toGet(if_id);
    interface MemoryClient memoryClient = fetchModule.memoryClient;
endmodule
