import CsrFile::*;
import Trap::*;
import DefaultValue::*;

//
// Epoch
//
typedef Bit#(1) Epoch;

//
// Instruction
//
typedef struct {
    Bit#(32) value;
} Instruction deriving(Bits, Eq, FShow);

instance DefaultValue #(Instruction);
    defaultValue = Instruction { 
        value:  32'b0000000_00000_00000_000_00000_0110011 // ADD x0, x0, x0
    };
endinstance

//
// ProgramCounter
//
typedef Bit#(xlen) ProgramCounter#(numeric type xlen);

//
// PC_IF
//
typedef struct {
    ProgramCounter#(xlen) pc;
    Maybe#(ProgramCounter#(xlen)) redirection;
    Epoch epoch;
} PC_IF#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue#(PC_IF#(xlen));
    defaultValue = PC_IF { 
        pc: 'h-1,
        redirection: tagged Invalid,
        epoch: defaultValue
    };
endinstance

//
// PipelineRegisterCommon
//
typedef struct {
    Instruction             ir;
    ProgramCounter#(xlen)   pc;
    Maybe#(Trap#(xlen))     trap;
} PipelineRegisterCommon#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue #(PipelineRegisterCommon#(xlen));
    defaultValue = PipelineRegisterCommon { 
        ir:         defaultValue,
        pc:         defaultValue,
        trap:       tagged Invalid
    };
endinstance

//
// IF_ID
//
typedef struct {
    PipelineRegisterCommon#(xlen) common;
    Epoch                         epoch;
    ProgramCounter#(xlen)         npc;     // Next program counter
} IF_ID#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue #(IF_ID#(xlen));
    defaultValue = IF_ID {
        common: defaultValue,
        epoch:  defaultValue,
        npc:    defaultValue
    };
endinstance

//
// ID_EX
//
typedef struct {
    PipelineRegisterCommon#(xlen) common;
    Epoch                         epoch;
    ProgramCounter#(xlen)         npc;          // Next program counter
    Bit#(xlen)                    a;            // Operand 1
    Bit#(xlen)                    b;            // Operand 2
    Bit#(xlen)                    imm;          // Sign extended immediate
    CsrReadResult#(xlen)          csr;          // CSR read result
} ID_EX#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue #(ID_EX#(xlen));
    defaultValue = ID_EX {
        common:   defaultValue,
        epoch:    defaultValue,
        npc:      defaultValue,
        a:        'h-1,
        b:        'h-1,
        imm:      'h-1,
        csr:      CsrReadResult {
            value: 'h-1,
            denied: False
        }
    };
endinstance

//
// EX_MEM
//
typedef struct {
    PipelineRegisterCommon#(xlen) common;
    Bit#(xlen)                    aluOutput;
    Bit#(xlen)                    storeValueOrCSRWriteback;
    Bool                          cond;        // Branch taken?
} EX_MEM#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue #(EX_MEM#(xlen));
    defaultValue = EX_MEM {
        common:                     defaultValue,
        aluOutput:                  'h-1,
        storeValueOrCSRWriteback:   'h-1,
        cond:                       False
    };
endinstance

//
// MEM_WB
//
typedef struct {
    PipelineRegisterCommon#(xlen) common;
    Bit#(xlen)                    gprWritebackValue;
    Bit#(xlen)                    csrWritebackValue;
} MEM_WB#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue #(MEM_WB#(xlen));
    defaultValue = MEM_WB {
        common:             defaultValue,
        gprWritebackValue:  'h-1,
        csrWritebackValue:  'h-1
    };
endinstance

//
// WB_OUT
//
typedef struct {
    PipelineRegisterCommon#(xlen) common;
    Bit#(xlen)                    csr_writeback_value;
    Bit#(xlen)                    gpr_writeback_value;
} WB_OUT#(numeric type xlen) deriving(Bits, Eq, FShow);

instance DefaultValue#(WB_OUT#(xlen));
    defaultValue = WB_OUT {
        common:              defaultValue,
        gpr_writeback_value: 'h-1,
        csr_writeback_value: 'h-1
    };
endinstance
