import MemoryAccessStage::*;
import IsaCfg::*;
import PipelineRegisters::*;
import RV_ISA::*;
import Trap::*;

import Assert::*;
import ClientServer::*;
import Connectable::*;
import FIFOF::*;
import GetPut::*;
import Memory::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) testNumber <- mkReg(0);

    // 32 bit
    IsaCfg#(32) rv32 = IsaCfg{
        extN: False,
        extS: False,
        extU: False
    };
    MemoryAccessModuleIfc#(32) memoryAccessModule32 <- mkMemoryAccessModule(rv32);

    FIFOF#(MemoryRequest#(32, 32)) memoryRequests32 <- mkUGFIFOF1();

    mkConnection(memoryAccessModule32.memoryClient.request, toPut(asIfc(memoryRequests32)));

    (* no_implicit_conditions *)
    rule test;
        EX_MEM#(32) ex_mem = defaultValue;

        case(testNumber)
            // Simple bubble passthrough
            0: begin
                let mem_wb <- memoryAccessModule32.step(ex_mem);
                dynamicAssert(mem_wb == defaultValue, "MemoryAccess - Bubble passthrough check");
            end

            default: begin
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_test_number;
        testNumber <= testNumber + 1;
    endrule
endmodule
