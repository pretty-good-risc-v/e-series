typedef struct {
    Bit#(32)   mvendorid;
    Bit#(xlen) marchid;
    Bit#(xlen) mimpid;
    Bit#(xlen) mhartid;
    Bit#(xlen) mconfigptr;
} MachineInformationCfg#(numeric type xlen);
