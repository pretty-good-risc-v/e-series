import DecodeStage::*;
import IsaCfg::*;
import MachineISA::*;

import Assert::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) stepNumber <- mkReg(0);

    IsaCfg#(32) rv32i_cfg = IsaCfg {
        extN: False,
        extS: False,
        extU: False
    };
    MachineISAIfc#(32) rv32i <- mkMachineISA(rv32i_cfg);

    IsaCfg#(64) rv64i_cfg = IsaCfg {
        extN: False,
        extS: False,
        extU: False
    };
    MachineISAIfc#(64) rv64i <- mkMachineISA(rv64i_cfg);

    (* no_implicit_conditions *)
    rule test;
        case(stepNumber)
            0: begin
                let misa = rv32i.pack;
                dynamicAssert(misa == 32'b01_0000_00000000000000000100000000, "MachineISA RV32I invalid");
            end

            1: begin
                let misa = rv64i.pack;
                $display("MISA: %b", misa);
                dynamicAssert(misa == 64'b10_000000000000000000000000000000000000_00000000000000000100000000, "MachineISA RV64I invalid");
            end

            default: begin
                dynamicAssert(stepNumber == 2, "MachineISA - not all tests run");
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_step_number;
        stepNumber <= stepNumber + 1;
    endrule
endmodule
