import IsaCfg::*;

import GetPut::*;

typedef Bit#(2) MXL;
MXL mxl_32bit  = 2'b01;
MXL mxl_64bit  = 2'b10;
MXL mxl_128bit = 2'b11;

typedef struct {
    Bool extZ;      // ** RESERVED **
    Bool extY;      // ** RESERVED **
    Bool extX;      // Non-standard extensions present
    Bool extW;      // ** RESERVED **
    Bool extV;      // Vector extension
    Bool extU;      // User mode implemented
    Bool extT;      // ** RESERVED **
    Bool extS;      // Supervisor mode implemented
    Bool extR;      // ** RESERVED **
    Bool extQ;      // Quad precision floating-point extension
    Bool extP;      // Packed-SIMD extension
    Bool extO;      // ** RESERVED **
    Bool extN;      // User level interrupts extension
    Bool extM;      // Integer multiply/divide extension
    Bool extL;      // ** RESERVED **
    Bool extK;      // ** RESERVED **
    Bool extJ;      // Dynamically translated language extension
    Bool extI;      // RV32I/64I/128I base ISA
    Bool extH;      // Hypervisor extension
    Bool extG;      // ** RESERVED **
    Bool extF;      // Single precision floating-point extension
    Bool extE;      // RV32E base ISA
    Bool extD;      // Double precision floating-point extension
    Bool extC;      // Compressed instruction extension
    Bool extB;      // Bit manipulation extension
    Bool extA;      // Atomic extension
} MachineISA deriving(Bits, Eq, FShow);

instance DefaultValue#(MachineISA);
    defaultValue = MachineISA {
        extA: False,
        extB: False,
        extC: False,
        extD: False,
        extE: False,
        extF: False,
        extG: False,
        extH: False,
        extI: True,     // RV32I/64I/128I base ISA
        extJ: False,
        extK: False,
        extL: False,
        extM: False,
        extN: False,
        extO: False,
        extP: False,
        extQ: False,
        extR: False,
        extS: False,
        extT: False,
        extU: False,
        extV: False,
        extW: False,
        extX: False,
        extY: False,
        extZ: False
    };
endinstance

interface MachineISAIfc#(numeric type xlen);
    method Bit#(xlen) pack;
    interface Get#(MachineISA) getMachineISA;
    interface Put#(MachineISA) putMachineISA;
endinterface

module mkMachineISA#(IsaCfg#(xlen) cfg)(MachineISAIfc#(xlen))
    provisos(
        Add#(a__, 26, xlen),
        Add#(b__, 2, xlen)
    );
    function MachineISA getMISA;
        MachineISA misa_ = defaultValue;

        // Check for user level interrupts
        if (cfg.extN) begin
            misa_.extN = True;
        end

        // Check for supervisor mode support.
        if (cfg.extS) begin
            misa_.extS = True;
        end

        // Check for user mode support.
        if (cfg.extU) begin
            misa_.extU = True;
        end

        return misa_;
    endfunction

    function MXL getMXL;
        MXL mxl = fromInteger((log2(valueof(xlen))) - 4); 
        return mxl;
    endfunction

    method Bit#(xlen) pack;
        Bit#(xlen) misa = 0;

        misa[valueof(xlen)-1:valueof(xlen)-2] = getMXL;

        Bit#(26) extensions = pack(getMISA);
        misa[25:0] = extensions;

        return misa;
    endmethod

    interface Get getMachineISA;
        method ActionValue#(MachineISA) get;
            return getMISA;
        endmethod
    endinterface

    interface Put putMachineISA;
        method Action put(MachineISA misa);
            // Writing to MISA not supported
        endmethod
    endinterface
endmodule
