import MachineStatus::*;
import IsaCfg::*;

import Assert::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) stepNumber <- mkReg(0);

    IsaCfg#(32) rv32icfg = defaultValue;
    MachineStatusIfc#(32) rv32i <- mkMachineStatus(rv32icfg); 

    IsaCfg#(64) rv64icfg = defaultValue;
    MachineStatusIfc#(64) rv64i <- mkMachineStatus(rv64icfg); 

    (* no_implicit_conditions *)
    rule test;
        case(stepNumber)
            default: begin
                dynamicAssert(stepNumber == 0, "MachineStatus - not all tests run");
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_step_number;
        stepNumber <= stepNumber + 1;
    endrule
endmodule
