typedef struct {
    Bit#(xlen) requestAddress;
} ICacheRequest#(numeric type xlen) deriving(Bits);

typedef struct {
    // data - data bytes returned from cache
    Bit#(TMul#(fetchByteCount,8)) data;
    // replay - if true, cache requests replay request.
    Bool replay;
    // access exception - if true, the request caused an access exception.    
    Bool accessException;   
} ICacheResponse#(numeric type fetchByteCount) deriving(Bits);

typedef struct {
    Bit#(xlen) requestAddress;
    Bool write;
} DCacheRequest#(numeric type xlen) deriving(Bits);
