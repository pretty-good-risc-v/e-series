import IsaCfg::*;
import PipelineRegisters::*;
import RV_ISA::*;
import Trap::*;

import Assert::*;
import ClientServer::*;
import FIFOF::*;
import GetPut::*;
import Memory::*;

interface MemoryAccessModuleIfc#(numeric type xlen);
    method ActionValue#(MEM_WB#(xlen)) step(EX_MEM#(xlen) ex_mem);
    interface MemoryClient#(xlen, xlen) memoryClient;
endinterface

module mkMemoryAccessModule#(IsaCfg#(xlen) cfg)(MemoryAccessModuleIfc#(xlen));
    // Memory request output
    Reg#(Bool) memoryRequestInFlight <- mkReg(False);
    Wire#(MemoryRequest#(xlen, xlen)) memoryRequest <- mkWire;

    // Memory response input (FIFO)
    // Note: This is an unguarded FIFO so status must be checked before attempting to enq() and deq().
    FIFOF#(MemoryResponse#(xlen)) memoryResponses <- mkUGFIFOF1;

    //
    // step - Execute memory access step by sending a data memory request (if needed) if one isn't already in flight and
    //        responding to any responses that have returned.  If a request is in flight, a bubble is returned.
    //
    method ActionValue#(MEM_WB#(xlen)) step(EX_MEM#(xlen) ex_mem);
        MEM_WB#(xlen) mem_wb = defaultValue;

        case (mem_wb.common.ir.value[6:0])
            matches 7'b0?00011: begin
                // LOAD/STORE
                
            end
            default: begin
                // All other opcodes
                mem_wb.common = ex_mem.common;
            end
        endcase
        
        return mem_wb;
    endmethod

    interface MemoryClient memoryClient;
        interface Get request = toGet(memoryRequest);
        interface Put response;
            method Action put(MemoryResponse#(xlen) response);
                dynamicAssert(memoryResponses.notFull, "MemoryStage - attempt to put a memory respnose on a full queue");
                memoryResponses.enq(response);
            endmethod
        endinterface
    endinterface
endmodule

interface MemoryAccessStageIfc#(numeric type xlen);
    interface Put#(EX_MEM#(xlen)) put;
    interface Get#(MEM_WB#(xlen)) get;
endinterface

module mkMemoryAccessStage#(IsaCfg#(xlen) cfg)(MemoryAccessStageIfc#(xlen));
    //
    // State
    //
    Wire#(EX_MEM#(xlen)) ex_mem <- mkWire;
    Reg#(MEM_WB#(xlen))  mem_wb <- mkRegU;
    MemoryAccessModuleIfc#(xlen) memoryAccessModule <- mkMemoryAccessModule(cfg);

    //
    // Rules
    //
    (* no_implicit_conditions *)
    rule step;
        let mem_wb_ <- memoryAccessModule.step(ex_mem);
        mem_wb <= mem_wb_;
    endrule

    //
    // Interfaces
    //
    interface Put put = toPut(asIfc(ex_mem));
    interface Get get = toGet(mem_wb);
endmodule
