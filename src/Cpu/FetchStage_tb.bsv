import FetchStage::*;
import IsaCfg::*;
import MemoryIO::*;
import PipelineRegisters::*;
import RV_ISA::*;
import Trap::*;

import Assert::*;
import ClientServer::*;
import Connectable::*;
import FIFOF::*;
import GetPut::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) stepNumber <- mkReg(0);

    // 32 bit
    IsaCfg#(32) rv32 = IsaCfg{
        extN: False,
        extS: False,
        extU: False
    };
    FetchModuleIfc#(32) fetchModule32 <- mkFetchModule(rv32);

    FIFOF#(InstructionMemoryRequest#(32)) memoryRequests32 <- mkUGFIFOF1();
    mkConnection(fetchModule32.memoryClient.request, toPut(asIfc(memoryRequests32)));

    (* no_implicit_conditions *)
    rule test;
        PC_IF#(32) pc_if = defaultValue;

        case(stepNumber)
            // Misaligned instruction trap
            0: begin
                pc_if.pc = 'h101;

                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id.common.pc == pc_if.pc, "Fetch - Misaligned instruction trap check - common.pc");
                dynamicAssert(if_id.common.ir == defaultValue, "Fetch - Misaligned instruction trap check - common.ir");
                dynamicAssert(isValid(if_id.common.trap), "Fetch - Misaligned instruction trap check - contains trap");
                dynamicAssert(if_id.common.trap.Valid.cause == exception_INSTRUCTION_ADDRESS_MISALIGNED, "Fetch - Misaligned instruction trap check - cause is misaligned address");
                dynamicAssert(if_id.npc == defaultValue, "Fetch - Misaligned instruction trap check - NPC incorrect");
            end

            // Memory request denied trap (step 1 - request submit)
            1: begin
                pc_if.pc = 'h100;

                // The fetch should proceed and return a bubble.
                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id == defaultValue, "Fetch - Memory request denied trap check - request should return a bubble");
            end

            // Memory request denied trap (step 2 - request receipt)
            2: begin
                pc_if.pc = 'h100;

                // Ensure the fetch returns a bubble while the memory request is in flight
                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id == defaultValue, "Fetch - Memory request denied trap check - request should return a bubble while fetch is in flight");

                dynamicAssert(memoryRequests32.notEmpty, "Fetch - Memory request denied trap check - memory request queue should not be empty");
                let memoryRequest = memoryRequests32.first;
                memoryRequests32.deq;

                dynamicAssert(memoryRequest.address == 'h100, "Fetch - Memory request denied trap check - memory request should have correct address");
                dynamicAssert(memoryRequest.byteen == 'b1111, "Fetch - Memory request denied trap check - memory request byte enable should be 'b1111");
                fetchModule32.memoryClient.response.put(FallibleMemoryResponse {
                    data: 'h-1,
                    accessFault: True
                });
            end

            // Memory request denied trap (step 3 - return trap check)
            3: begin
                pc_if.pc = 'h100;

                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id.common.pc == pc_if.pc, "Fetch - Memory request denied trap check - common.pc");
                dynamicAssert(if_id.common.ir == defaultValue, "Fetch - Memory request denied trap check - common.ir");
                dynamicAssert(isValid(if_id.common.trap), "Fetch - Memory request denied trap check - contains trap");
                dynamicAssert(if_id.common.trap.Valid.cause == exception_INSTRUCTION_ACCESS_FAULT, "Memory request denied trap check - cause is access fault");
                dynamicAssert(if_id.npc == 'h104, "Fetch - Misaligned instruction trap check - NPC incorrect");
                dynamicAssert(if_id.epoch == defaultValue, "Fetch - Misaligned instruction trap check - Verify epoch unchanged");
            end

            // Normal memory request (step 1 - request submit)
            4: begin
                pc_if.pc = 'h100;

                // The fetch should proceed and return a bubble.
                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id == defaultValue, "Fetch - Normal request - request should return a bubble");
            end

            // Normal memory request (step 2 - request receipt)
            5: begin
                pc_if.pc = 'h100;

                // Ensure the fetch returns a bubble while the memory request is in flight
                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id == defaultValue, "Fetch - Normal request - request should return a bubble while fetch is in flight");

                dynamicAssert(memoryRequests32.notEmpty, "Fetch - Normal request - memory request queue should not be empty");
                let memoryRequest = memoryRequests32.first;
                memoryRequests32.deq;

                dynamicAssert(memoryRequest.address == 'h100, "Fetch - Normal request - memory request should have correct address");
                dynamicAssert(memoryRequest.byteen == 'b1111, "Fetch - Normal request - memory request byte enable should be 'b1111");
                fetchModule32.memoryClient.response.put(FallibleMemoryResponse {
                    data: 'haabb_ccdd,
                    accessFault: False
                });
            end

            // Normal memory request (step 3 - return value check)
            6: begin
                pc_if.pc = 'h100;

                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id.common.pc == pc_if.pc, "Fetch - Normal request - common.pc");
                dynamicAssert(!isValid(if_id.common.trap), "Fetch - Normal request - contains no trap");
                dynamicAssert(if_id.common.ir.value == 'haabb_ccdd, "Fetch - Normal request - contains expected instruction data");
                dynamicAssert(if_id.npc == 'h104, "Fetch - Normal request - NPC incorrect");
                dynamicAssert(if_id.epoch == defaultValue, "Fetch - Normal request - Verify epoch unchanged");
            end

            // Redirect handling check (step 1 - request submit)
            7: begin
                pc_if.pc = 'h100;
                pc_if.redirection = tagged Valid 'h8000;

                // The fetch should proceed and return a bubble.
                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id == defaultValue, "Fetch - Redirect check - request should return a bubble");
            end

            // Redirect handling check (step 2 - request receipt)
            8: begin
                pc_if.pc = 'h100;

                // Ensure the fetch returns a bubble while the memory request is in flight
                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id == defaultValue, "Fetch - Redirect check - request should return a bubble while fetch is in flight");

                dynamicAssert(memoryRequests32.notEmpty, "Fetch - Redirect check - memory request queue should not be empty");
                let memoryRequest = memoryRequests32.first;
                memoryRequests32.deq;

                dynamicAssert(memoryRequest.address == 'h8000, "Fetch - Redirect check - memory request should have correct address");
                dynamicAssert(memoryRequest.byteen == 'b1111, "Fetch - Redirect check - memory request byte enable should be 'b1111");
                fetchModule32.memoryClient.response.put(FallibleMemoryResponse {
                    data: 'haabb_ccee,
                    accessFault: False
                });
            end

            // Redirect handling check (step 3 - return value check)
            9: begin
                pc_if.pc = 'h100;

                let if_id <- fetchModule32.step(pc_if);
                dynamicAssert(if_id.common.pc == 'h8000, "Fetch - Redirect check - common.pc");
                dynamicAssert(!isValid(if_id.common.trap), "Fetch - Redirect check - contains no trap");
                dynamicAssert(if_id.common.ir.value == 'haabb_ccee, "Fetch - Redirect check - contains expected instruction data");
                dynamicAssert(if_id.npc == 'h8004, "Fetch - Redirect check - NPC incorrect");
                dynamicAssert(if_id.epoch == 'h1, "Fetch - Redirect check - Verify epoch changed");
            end

            default: begin
                dynamicAssert(stepNumber == 10, "Fetch - not all tests run");
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_step_number;
        stepNumber <= stepNumber + 1;
    endrule
endmodule
