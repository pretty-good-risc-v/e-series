import DecodeStage::*;
import GprFile::*;
import IsaCfg::*;
import PipelineRegisters::*;
import RV_ISA::*;
import Trap::*;

import Assert::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) stepNumber <- mkReg(0);

    // 32 bit
    IsaCfg#(32) rv32 = IsaCfg{
        extN: False,
        extS: False,
        extU: False
    };
    DecodeModuleIfc#(32) decodeModule32 <- mkDecodeModule(rv32);

    //
    // GPR
    //
    GprFileCfg#(32) gprFileCfg32 = GprFileCfg {
    };
    GprFileIfc#(32) gprFile32 <- mkGprFile(gprFileCfg32);

    (* no_implicit_conditions *)
    rule test;
        IF_ID#(32) if_id = defaultValue;

        case(stepNumber)
            default: begin
                dynamicAssert(stepNumber == 0, "Decode - not all tests run");
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_step_number;
        stepNumber <= stepNumber + 1;
    endrule
endmodule
