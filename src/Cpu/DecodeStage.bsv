import CsrFile::*;
import IsaCfg::*;
import GprFile::*;
import PipelineRegisters::*;
import RV_ISA::*;
import Trap::*;

import GetPut::*;

interface DecodeModuleIfc#(numeric type xlen);
    method ActionValue#(ID_EX#(xlen)) step(
        IF_ID#(xlen) if_id, 
        GprReadPort#(xlen) gprReadPort1, 
        GprReadPort#(xlen) gprReadPort2,
        CsrReadPort#(xlen) csrReadPort
    );
endinterface

module mkDecodeModule#(IsaCfg#(xlen) cfg)(DecodeModuleIfc#(xlen))
    provisos(
        Add#(a__, 20, xlen),
        Add#(b__, 5, xlen),
        Add#(c__, 13, xlen),
        Add#(d__, 21, xlen),
        Add#(e__, 12, xlen)
    );
    function RVGPRIndex extractRs1(Instruction inst);
        return inst.value[19:15];
    endfunction

    function RVGPRIndex extractRs2(Instruction inst);
        return inst.value[24:20];
    endfunction

    function RVCSRIndex extractCsrIndex(Instruction inst);
        return inst.value[31:20];
    endfunction

    function Bit#(xlen) decodeImmediate(Instruction inst);
        return case(inst.value[6:0])
            7'b0110111: begin // LUI
                signExtend(inst.value[31:12]);
            end

            7'b0010111: begin // AUIPC
                signExtend(inst.value[31:12]);
            end

            7'b1101111: begin // JAL
                signExtend({
                    inst.value[31],
                    inst.value[19:12],
                    inst.value[20],
                    inst.value[30:21],
                    1'b0
                });
            end

            7'b1100111: begin // JALR
                signExtend(inst.value[31:20]);
            end

            7'b1100011: begin // BRANCH
                signExtend({
                    inst.value[31],
                    inst.value[7],
                    inst.value[30:25],
                    inst.value[11:8],
                    1'b0
                });
            end

            7'b0000011: begin // LOAD
                signExtend(inst.value[31:20]);
            end

            7'b0100011: begin // STORE
                signExtend({
                    inst.value[31:25],
                    inst.value[11:7]
                });
            end

            7'b0010011: begin // OPIMM
                signExtend(inst.value[31:20]);
            end

            7'b1110011: begin // SYSTEM (CSR)
                zeroExtend(inst.value[19:15]);  // UIMM stored in RS1
            end

            default: begin
                0;
            end
        endcase;
    endfunction

    method ActionValue#(ID_EX#(xlen)) step(
        IF_ID#(xlen) if_id, 
        GprReadPort#(xlen) gprReadPort1, 
        GprReadPort#(xlen) gprReadPort2,
        CsrReadPort#(xlen) csrReadPort
    );
        ID_EX#(xlen) id_ex = defaultValue;

        id_ex.common = if_id.common;
        id_ex.epoch = if_id.epoch;
        id_ex.npc = if_id.npc;

        // Read GPRs
        let rs1 = extractRs1(if_id.common.ir);
        let rs2 = extractRs2(if_id.common.ir);
        id_ex.a = gprReadPort1.read(rs1);
        id_ex.b = gprReadPort2.read(rs2);

        // Decode immediate
        id_ex.imm = decodeImmediate(if_id.common.ir);

        // CSR read
        let csrIndex = extractCsrIndex(if_id.common.ir);
        id_ex.csr <- csrReadPort.read(csrIndex);

        return id_ex;
    endmethod
endmodule

interface DecodeStageIfc#(numeric type xlen);
    interface Put#(IF_ID#(xlen)) put;
    interface Get#(ID_EX#(xlen)) get;
endinterface

module mkDecodeStage#(IsaCfg#(xlen) cfg, 
                      GprReadPort#(xlen) gprReadPort1, 
                      GprReadPort#(xlen) gprReadPort2, 
                      CsrReadPort#(xlen) csrReadPort)(DecodeStageIfc#(xlen))
    provisos(
        Add#(a__, 20, xlen),
        Add#(b__, 5, xlen),
        Add#(c__, 13, xlen),
        Add#(d__, 21, xlen),
        Add#(e__, 12, xlen)
    );

    //
    // State
    //
    Wire#(IF_ID#(xlen)) if_id <- mkWire;
    Reg#(ID_EX#(xlen))  id_ex <- mkRegU;
    DecodeModuleIfc#(xlen) decodeModule <- mkDecodeModule(cfg);

    //
    // Rules
    //
    (* no_implicit_conditions *)
    rule step;
        let id_ex_ <- decodeModule.step(if_id, gprReadPort1, gprReadPort2, csrReadPort);
        id_ex <= id_ex_;
    endrule

    //
    // Interfaces
    //
    interface Put put = toPut(asIfc(if_id));
    interface Get get = toGet(id_ex);
endmodule
