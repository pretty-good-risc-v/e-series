typedef struct {
    Bit#(4) cause;  // Either exception or interrupt
    Bool isInterrupt;
    Bit#(xlen) tval;
} Trap#(numeric type xlen) deriving(Bits, Eq, FShow);

interface TrapControllerIfc#(numeric type xlen);
    method ActionValue#(Bit#(xlen)) beginTrap(Bit#(xlen) trap_program_counter, Trap#(xlen) trap);
    method ActionValue#(Bit#(xlen)) endTrap;
endinterface
