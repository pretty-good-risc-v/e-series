import CsrFile::*;
import IsaCfg::*;

import Assert::*;

module mkTopModule(Empty);
    Reg#(Bit#(20)) stepNumber <- mkReg(0);

    (* no_implicit_conditions *)
    rule test;
        case(stepNumber)
            default: begin
                dynamicAssert(stepNumber == 0, "CsrFile - not all tests run");
                $display(">>>PASS");
                $finish();
            end
        endcase
    endrule

    rule increment_step_number;
        stepNumber <= stepNumber + 1;
    endrule
endmodule
