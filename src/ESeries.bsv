//!submodule Cpu
//!submodule Memory
//!submodule Util
import Soc::*;

//!topmodule mkE001
module mkE001(Soc#(32, 32));
    SocCfg#(32, 32) cfg = SocCfg {
        cpu_cfg: CpuCfg {
            initial_program_counter: 'h8000_0000
        }
    };
    let soc <- mkSoc(cfg);
endmodule

//!topmodule mkE003
module mkE003(Soc#(64, 32));
    SocCfg#(64, 32) cfg = SocCfg{
        cpu_cfg: CpuCfg {
            initial_program_counter: 'h8000_0000
        }
    };
    let soc <- mkSoc(cfg);
endmodule
